package com.tecso.technicaltest.config.swagger;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class Swagger2Config  implements WebMvcConfigurer
{

	@Bean
	public Docket swaggerPersona()
	{
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("Account")
				.select()
				.paths(regex("/account/*.*"))
				.apis(RequestHandlerSelectors.basePackage("com.tecso.technicaltest.controller"))
				.build()
				.apiInfo(apiInfo()).pathMapping("/");
	}
	
	@Bean
	public Docket swaggerCategoria()
	{
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("Account Movement")
				.select()
				.paths(regex("/accountMovement/*.*"))
				.apis(RequestHandlerSelectors.basePackage("com.tecso.technicaltest.controller"))
				.build()
				.apiInfo(apiInfo()).pathMapping("/");
	}


	private ApiInfo apiInfo()
	{
		return new ApiInfoBuilder()
				.title("Challenge 2 Tecso")
				.description("Challenge 2 Tecso")
				.license("Apache License Version 2.0")
				.version("1.0")
				.build();
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/v1/api-docs", "/v1/api-docs");
		registry.addRedirectViewController("/swagger-resources/configuration/ui", "/swagger-resources/configuration/ui");
		registry.addRedirectViewController("/swagger-resources/configuration/security", "/swagger-resources/configuration/security");
		registry.addRedirectViewController("/swagger-resources", "/swagger-resources");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

}