package com.tecso.technicaltest.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tecso.technicaltest.dto.AccountDto;
import com.tecso.technicaltest.dto.BaseResponseDto;
import com.tecso.technicaltest.service.AccountService;
import com.tecso.technicaltest.util.SystemMessage;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Julian Valencia
 * 26/05/2019
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/account")
@Api( value = "Account creation services.", tags = { "Accounts" })
public class AccountController {
	
	private AccountService  accountService;
	
	private static final Logger LOGGER = LogManager.getLogger(AccountController.class.getName());
	
	public AccountController(AccountService accountService) {
		this.accountService = accountService;
	}	
	
	@ApiOperation(value = "Create an account.", response = BaseResponseDto.class,tags = { "Account" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Account created.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Unable to create account. Validation exception", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Internal error.", response = BaseResponseDto.class), 
	})
	@PostMapping(value = "/create",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> saveAccount(@RequestBody(required = true) AccountDto accountDto) {
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			accountService.saveAccount(accountDto);
			response.setResponseMessage(SystemMessage.STATUS_OK);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response, headers, httpStatus);
	}
	
	@ApiOperation(value = "Get all accounts.", response = BaseResponseDto.class,tags = { "Get all accounts" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Successful search.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Unable to search accounts. Validation exception", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Internal error.", response = BaseResponseDto.class), 
	})
    @GetMapping(value = "/getAllAccounts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> getAllAccount() {
		
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			List<AccountDto> accounts = accountService.getAccountList();
			response.setResponseMessage(SystemMessage.STATUS_OK);
			response.setResponseBody(accounts);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response,headers,httpStatus);
	}
	
	@ApiOperation(value = "Delete an account", response = BaseResponseDto.class,tags = { "Delete account" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Account deleted",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Unable to delete an account. Validation exception", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Internal error.", response = BaseResponseDto.class), 
	})
	@DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> deleteAccount(@PathVariable long id) {
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		
		try {
			accountService.deleteAccount(id);
			response.setResponseMessage(SystemMessage.STATUS_OK);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response,headers,httpStatus);
	} 

}
