package com.tecso.technicaltest.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tecso.technicaltest.dto.AccountMovementDto;
import com.tecso.technicaltest.dto.BaseResponseDto;
import com.tecso.technicaltest.service.AccountMovementService;
import com.tecso.technicaltest.util.SystemMessage;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Julian Valencia
 * 26/05/2019
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/accountMovement")
@Api( value = "AccountMovement creation services.", tags = { "AccountMovements" })
public class AccountMovementController {
	
	private AccountMovementService  accountMovementService;
	
	private static final Logger LOGGER = LogManager.getLogger(AccountMovementController.class.getName());
	
	public AccountMovementController(AccountMovementService accountMovementService) {
		this.accountMovementService = accountMovementService;
	}	
	
	@ApiOperation(value = "Create an accountMovement.", response = BaseResponseDto.class,tags = { "AccountMovement" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "AccountMovement created.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Unable to create an account movement. Validation exception", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Internal error.", response = BaseResponseDto.class), 
	})
	@PostMapping(value = "/create",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> saveAccountMovement(@RequestBody(required = true) AccountMovementDto accountMovementDto) {
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			accountMovementService.updateAccountBalance(accountMovementDto);
			response.setResponseMessage(SystemMessage.STATUS_OK);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response, headers, httpStatus);
	}
	
	@ApiOperation(value = "Get all account movements.", response = BaseResponseDto.class,tags = { "Get all account movements" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Successful search.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Unable to search accountMovements. Validation exception", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Internal error.", response = BaseResponseDto.class), 
	})
    @GetMapping(value = "/getAllAccountMovements", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> getAllAccountMovement(@RequestBody(required = true) AccountMovementDto accountMovementDto) {
		
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			List<AccountMovementDto> accountMovements = accountMovementService.getMovementsByAccount(accountMovementDto.getIdaccount());
			response.setResponseMessage(SystemMessage.STATUS_OK);
			response.setResponseBody(accountMovements);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response,headers,httpStatus);
	}

}
