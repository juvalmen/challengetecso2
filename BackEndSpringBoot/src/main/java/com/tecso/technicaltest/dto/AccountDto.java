package com.tecso.technicaltest.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Julian Valencia
 * 25/05/2019
 */
public class AccountDto implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 284014210158016584L;
	
	private Long idaccount;
	private String accountNumber;
	private String currencyType;
	private String currencyTypeDesc;
	private Double balance;
	private Date modificationDate; 
    private Date creationDate;
	
	public Long getIdaccount() {
		return idaccount;
	}
	public void setIdaccount(Long idaccount) {
		this.idaccount = idaccount;
	}	
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public String getCurrencyTypeDesc() {
		return currencyTypeDesc;
	}
	public void setCurrencyTypeDesc(String currencyTypeDesc) {
		this.currencyTypeDesc = currencyTypeDesc;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	   
}