package com.tecso.technicaltest.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Julian Valencia
 * 25/05/2019
 */
public class AccountMovementDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 343432101580456765L;
	
	private Long idaccountmovement;
	private Long idaccount;
	private String accountNumber;
	private String accountCurrencyType;
	private Date movementDate;
	private String movementType;
	private String movementTypeDesc;
	private String description;
	private Double movementValue;
    private Date creationDate;
	
	public Long getIdaccountmovement() {
		return idaccountmovement;
	}
	public void setIdaccountmovement(Long idaccountmovement) {
		this.idaccountmovement = idaccountmovement;
	}
	public Long getIdaccount() {
		return idaccount;
	}
	public void setIdaccount(Long idaccount) {
		this.idaccount = idaccount;
	}
	public Date getMovementDate() {
		return movementDate;
	}
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	public String getMovementType() {
		return movementType;
	}
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	public String getMovementTypeDesc() {
		return movementTypeDesc;
	}
	public void setMovementTypeDesc(String movementTypeDesc) {
		this.movementTypeDesc = movementTypeDesc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getMovementValue() {
		return movementValue;
	}
	public void setMovementValue(Double movementValue) {
		this.movementValue = movementValue;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountCurrencyType() {
		return accountCurrencyType;
	}
	public void setAccountCurrencyType(String accountCurrencyType) {
		this.accountCurrencyType = accountCurrencyType;
	}

	
		   
}