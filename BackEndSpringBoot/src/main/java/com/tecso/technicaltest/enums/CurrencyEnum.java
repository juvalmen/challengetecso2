package com.tecso.technicaltest.enums;

public enum CurrencyEnum {
	DOLAR("1"),
	EURO("2"),
	PESO("3");
	private String type;	
	private CurrencyEnum(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
