package com.tecso.technicaltest.enums;

public enum MovementEnum {
	DEBITO("1"),
	CREDITO("2");
	private String type;	
	private MovementEnum(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
