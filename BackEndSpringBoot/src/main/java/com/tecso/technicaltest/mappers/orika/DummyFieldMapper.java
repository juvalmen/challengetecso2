package com.tecso.technicaltest.mappers.orika;

import org.springframework.stereotype.Component;

import com.tecso.technicaltest.dto.AccountDto;
import com.tecso.technicaltest.dto.AccountMovementDto;
import com.tecso.technicaltest.model.Account;
import com.tecso.technicaltest.model.AccountMovement;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
@Component("dummyFieldMapper")
public class DummyFieldMapper extends ConfigurableMapper {

    @Override
	public void configure(MapperFactory factory) {
    	
    	factory = new DefaultMapperFactory.Builder().build();
    	
    	factory.classMap(Account.class, AccountDto.class)
    	.register();
    	
    	factory.classMap(AccountMovement.class, AccountMovementDto.class)
    	.field("account.idaccount", "idaccount")
    	.field("account.accountNumber", "accountNumber")
    	.register();
    }
  

}

