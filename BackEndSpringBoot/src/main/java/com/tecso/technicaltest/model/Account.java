package com.tecso.technicaltest.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.tecso.technicaltest.enums.CurrencyEnum;

@Entity
public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4117822122291207071L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idaccount;
	
	@Column(name = "account_number", nullable = false, length = 10)
	private String accountNumber;
	
	@Column(name = "currency_type", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private CurrencyEnum currencyType; 
	
	private Double balance;
	
	@Column(name = "modification_date", nullable = true, length = 13)
    private Date modificationDate; 

	@Column(name = "creation_date", nullable = true, length = 13)
    private Date creationDate;

	public Long getIdaccount() {
		return idaccount;
	}

	public void setIdaccount(Long idaccount) {
		this.idaccount = idaccount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public CurrencyEnum getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(CurrencyEnum currencyType) {
		this.currencyType = currencyType;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	

}