package com.tecso.technicaltest.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.tecso.technicaltest.enums.MovementEnum;

@Entity
@Table(name = "accountmovement")
public class AccountMovement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3443434222954354233L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idaccountmovement;
	
	@ManyToOne
	@JoinColumn(name="idaccount", nullable = false)
	private Account account;

	@Column(name = "movement_date", nullable = false)
	private Date movementDate;
	
	@Column(name = "movement_type", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private MovementEnum movementType; 
	
	private String description;

	@Column(name = "value", nullable = false, length = 10, columnDefinition="Decimal(10,2)")
	private Double movementValue;

	@Column(name = "creation_date", nullable = false, length = 13)
    private Date creationDate;

	public Long getIdaccountmovement() {
		return idaccountmovement;
	}

	public void setIdaccountmovement(Long idaccountmovement) {
		this.idaccountmovement = idaccountmovement;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getMovementDate() {
		return movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public MovementEnum getMovementType() {
		return movementType;
	}

	public void setMovementType(MovementEnum movementType) {
		this.movementType = movementType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getMovementValue() {
		return movementValue;
	}

	public void setMovementValue(Double movementValue) {
		this.movementValue = movementValue;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


}