package com.tecso.technicaltest.repository.jdbc;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tecso.technicaltest.model.Account;
import com.tecso.technicaltest.model.AccountMovement;

@Repository
public class JdbcAccountMovementRepository {

	EntityManager entityManager;

	@Autowired
	public JdbcAccountMovementRepository(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	/**
	 * 
	 * @author Julian Valencia
	 * 25/05/2019
	 */
	public List<AccountMovement> getMovementsByAccount(Long idAccount) {
	
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<AccountMovement> query = cb.createQuery(AccountMovement.class);
		Root<AccountMovement> root = query.from(AccountMovement.class);
		Join<AccountMovement, Account> join = root.join("account");

		query = query.select(root).where(
				cb.equal(join.get("idaccount"), idAccount))
				.orderBy(cb.desc(root.get("movementDate")));
		
		return entityManager.createQuery(query).getResultList();

	}
	

}
