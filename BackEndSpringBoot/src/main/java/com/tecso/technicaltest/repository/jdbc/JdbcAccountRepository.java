package com.tecso.technicaltest.repository.jdbc;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tecso.technicaltest.model.Account;

@Repository
public class JdbcAccountRepository {

	private JdbcTemplate jdbcTemplate;
	EntityManager entityManager;

	@Autowired
	public JdbcAccountRepository(JdbcTemplate jdbcTemplate, 
			EntityManager entityManager) {
		this.jdbcTemplate = jdbcTemplate;
		this.entityManager = entityManager;
	}

	/**
	 * 
	 * @author Julian Valencia
	 * 28/05/2019
	 */
	public List<Account> getAccountByNumber(String accountNumber) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<Account> q = cb.createQuery(Account.class);
		Root<Account> c = q.from(Account.class);
		q.select(c).where(cb.equal(c.get("accountNumber"), accountNumber));

		return entityManager.createQuery(q).getResultList();
	}

	/**
	 * @author Julian Valencia
	 * @param idaccount
	 * @return
	 */
	public boolean countAccountMovement(Long idaccount) {

		String sql = "SELECT count(1) FROM accountmovement WHERE idaccount = ?";
		boolean result = false;

		int count = jdbcTemplate.queryForObject(sql, new Object[] { idaccount }, Integer.class);

		if (count > 0) {
			result = true;
		}

		return result;
	}
	
	/**
	 * 
	 * @author Julian Valencia
	 * 18/05/2019
	 */
	public List<Account> getAllAccounts() {
		CriteriaQuery<Account> q = entityManager.getCriteriaBuilder().createQuery(Account.class);
		q.select(q.from(Account.class));
		List<Account> listOfAccounts = entityManager.createQuery(q).getResultList();
		return listOfAccounts;
	}


}
