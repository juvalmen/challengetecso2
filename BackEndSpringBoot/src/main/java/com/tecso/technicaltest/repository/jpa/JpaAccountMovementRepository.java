package com.tecso.technicaltest.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tecso.technicaltest.model.AccountMovement;

public interface JpaAccountMovementRepository extends JpaRepository<AccountMovement, Long> {

}
