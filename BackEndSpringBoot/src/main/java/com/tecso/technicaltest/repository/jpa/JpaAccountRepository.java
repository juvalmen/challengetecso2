package com.tecso.technicaltest.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tecso.technicaltest.model.Account;

public interface JpaAccountRepository extends JpaRepository<Account, Long> {

}
