package com.tecso.technicaltest.service;

import java.util.List;

import com.tecso.technicaltest.dto.AccountMovementDto;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

public interface AccountMovementService {
	List<AccountMovementDto> getMovementsByAccount(Long idAccount) throws TecsoCustomException;
	boolean updateAccountBalance(AccountMovementDto accountMovementDto) throws TecsoCustomException;
}
