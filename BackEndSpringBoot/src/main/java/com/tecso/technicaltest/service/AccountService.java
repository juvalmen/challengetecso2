package com.tecso.technicaltest.service;

import java.util.List;

import com.tecso.technicaltest.dto.AccountDto;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

public interface AccountService {
	List<AccountDto> getAccountList() throws TecsoCustomException;
	boolean saveAccount(AccountDto accountDto) throws TecsoCustomException;
    boolean deleteAccount(Long id) throws TecsoCustomException;
	boolean getAccountByNumber(String accountNumber) throws TecsoCustomException;
}
