package com.tecso.technicaltest.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.dto.AccountDto;
import com.tecso.technicaltest.dto.AccountMovementDto;
import com.tecso.technicaltest.enums.CurrencyEnum;
import com.tecso.technicaltest.enums.MovementEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Account;
import com.tecso.technicaltest.model.AccountMovement;
import com.tecso.technicaltest.repository.jdbc.JdbcAccountMovementRepository;
import com.tecso.technicaltest.repository.jpa.JpaAccountMovementRepository;
import com.tecso.technicaltest.repository.jpa.JpaAccountRepository;
import com.tecso.technicaltest.service.AccountMovementService;
import com.tecso.technicaltest.util.DateUtils;
import com.tecso.technicaltest.util.validations.AccountMovementValidationMessages;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

/**
 * 
 * @author Julian Valencia
 * 26/05/2019
 */
@Service
public class AccountMovementServiceImpl implements AccountMovementService {

	private JpaAccountRepository jpaAccountRepository;
	private JpaAccountMovementRepository jpaAccountMovementRepository;
	private JdbcAccountMovementRepository jdbcAccountMovementRepository;
	private DummyFieldMapper dummyFieldMapper;
	
	private static final Logger LOGGER = LogManager.getLogger(AccountMovementServiceImpl.class.getName());
	
	// Length fields
	private final int DESCRIPTION_MAX_LENGTH = 200;
	private final int MAX_VALUE_DOLAR = 300;
	private final int MAX_VALUE_EURO = 150;
	private final int MAX_VALUE_PESO = 1000;

	/**
	 * 
	 * @author Julian Valencia
	 * 26/05/2019
	 * @param jpaAccountMovementRepository
	 * @param jdbcAccountMovementRepository
	 * @param jpaAccountRepository
	 * @param dummyFieldMapper
	 */
	@Autowired
	public AccountMovementServiceImpl(JpaAccountMovementRepository jpaAccountMovementRepository, 
			JdbcAccountMovementRepository jdbcAccountMovementRepository,
			JpaAccountRepository jpaAccountRepository,
			DummyFieldMapper dummyFieldMapper) {
		this.jpaAccountMovementRepository = jpaAccountMovementRepository;
		this.jdbcAccountMovementRepository = jdbcAccountMovementRepository;
		this.jpaAccountRepository = jpaAccountRepository;
		this.dummyFieldMapper = dummyFieldMapper;
	}
	
	/**
	 * 
	 */
	@Override
	public List<AccountMovementDto> getMovementsByAccount(Long idAccount) throws TecsoCustomException {		
		List<AccountMovement> accountMovements = jdbcAccountMovementRepository.getMovementsByAccount(idAccount);
		List<AccountMovementDto> accountMovementsDto = new ArrayList<AccountMovementDto>();
		
		for (AccountMovement accountMovement : accountMovements) {
			AccountMovementDto dto = dummyFieldMapper.map(accountMovement, AccountMovementDto.class);
			
			dto.setIdaccount(accountMovement.getAccount().getIdaccount());
			dto.setAccountNumber(accountMovement.getAccount().getAccountNumber());
			dto.setAccountCurrencyType(accountMovement.getAccount().getCurrencyType().name());	
			dto.setMovementTypeDesc(accountMovement.getMovementType().name());	
			accountMovementsDto.add(dto);
		}
			
		return accountMovementsDto;
	}

	/**
	 * 
	 */
	@Transactional(rollbackFor=Exception.class)
	@Override
	public boolean updateAccountBalance(AccountMovementDto accountMovementDto) throws TecsoCustomException {
		try {
		
			if (accountMovementDto.getIdaccount() == null) {
				throw new TecsoCustomException(AccountMovementValidationMessages.ACCOUNT_ID_NULL);
			}
			
			Optional<Account> account = jpaAccountRepository.findById(accountMovementDto.getIdaccount());
			if(!account.isPresent()) {				
				throw new TecsoCustomException(AccountMovementValidationMessages.ACCOUNT_DOESNT_EXIST);
			}
			
			AccountDto accountDto = dummyFieldMapper.map(account.get(), AccountDto.class);
			
			if (accountMovementDto.getDescription() == null || accountMovementDto.getDescription().isEmpty()) {
				throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_DESCRIPTION_EMPTY);
			}
			
			if (accountMovementDto.getDescription().length() > DESCRIPTION_MAX_LENGTH) {
				throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_DESCRIPTION_LENGTH);
			}

			if (accountMovementDto.getMovementValue() == null || accountMovementDto.getMovementValue() < 0) {
				throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_WRONG_VALUE);
			}
			
			if (accountMovementDto.getMovementType() == null || accountMovementDto.getMovementType().isEmpty()) {
				throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_TYPE_EMPTY);
			}

			if (MovementEnum.valueOf(accountMovementDto.getMovementType()) == null) {
				
			}
			
			accountMovementUpdateValidation(accountDto, accountMovementDto);
			
			Account accountModified = dummyFieldMapper.map(accountDto, Account.class);
			accountModified.setCurrencyType(CurrencyEnum.valueOf(accountDto.getCurrencyType()));
			accountModified.setModificationDate(DateUtils.getDateUTCFormat());
			jpaAccountRepository.save(accountModified);
			
			AccountMovement newAccountMovement = dummyFieldMapper.map(accountMovementDto, AccountMovement.class);
			newAccountMovement.setAccount(accountModified);
			newAccountMovement.setMovementType(MovementEnum.valueOf(accountMovementDto.getMovementType()));
			newAccountMovement.setMovementDate(DateUtils.getDateUTCFormat());
			newAccountMovement.setCreationDate(DateUtils.getDateUTCFormat());
			jpaAccountMovementRepository.save(newAccountMovement);			
			
		
		} catch (IllegalArgumentException e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_TYPE_DOESNT_EXIST);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(e.getMessage());
		}
		return true;
	}

	/**
	 * 
	 * @param accountDto
	 * @param accountMovementDto
	 * @throws TecsoCustomException
	 */
	private void accountMovementUpdateValidation(AccountDto accountDto, AccountMovementDto accountMovementDto) throws TecsoCustomException {

		if(accountMovementDto.getMovementType().equals(MovementEnum.CREDITO.name())) {
			Double movevementTransactionValue = accountDto.getBalance()-accountMovementDto.getMovementValue();
			if(movevementTransactionValue < 0) {
				if(accountDto.getCurrencyType().equals(CurrencyEnum.DOLAR.name())) {
					if(movevementTransactionValue*(-1) > MAX_VALUE_DOLAR) {
						throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_MAX_VALUE_DOLAR);
					}
				}
				if(accountDto.getCurrencyType().equals(CurrencyEnum.EURO.name())) {
					if(movevementTransactionValue*(-1) > MAX_VALUE_EURO) {
						throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_MAX_VALUE_EURO);
					}
				}
				if(accountDto.getCurrencyType().equals(CurrencyEnum.PESO.name())) {
					if(movevementTransactionValue*(-1) > MAX_VALUE_PESO) {
						throw new TecsoCustomException(AccountMovementValidationMessages.MOVEMENT_MAX_VALUE_PESO);
					}
				}
			}
			accountDto.setBalance(accountDto.getBalance()-accountMovementDto.getMovementValue());
		} else {
			accountDto.setBalance(accountDto.getBalance()+accountMovementDto.getMovementValue());
		}		
		
	}

}
