package com.tecso.technicaltest.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecso.technicaltest.dto.AccountDto;
import com.tecso.technicaltest.enums.CurrencyEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Account;
import com.tecso.technicaltest.repository.jdbc.JdbcAccountRepository;
import com.tecso.technicaltest.repository.jpa.JpaAccountRepository;
import com.tecso.technicaltest.service.AccountService;
import com.tecso.technicaltest.util.DateUtils;
import com.tecso.technicaltest.util.validations.AccountValidationMessages;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@Service
public class AccountServiceImpl implements AccountService {

	private JpaAccountRepository jpaAccountRepository;
	private JdbcAccountRepository jdbcAccountRepository;
	private DummyFieldMapper dummyFieldMapper;
	
	private static final Logger LOGGER = LogManager.getLogger(AccountServiceImpl.class.getName());
	
	// Length fields
	private final int ACCOUNT_MAX_LENGTH = 10;

	@Autowired
	public AccountServiceImpl(JpaAccountRepository jpaAccountRepository, JdbcAccountRepository jdbcAccountRepository,
			DummyFieldMapper dummyFieldMapper) {
		this.jpaAccountRepository = jpaAccountRepository;
		this.jdbcAccountRepository = jdbcAccountRepository;
		this.dummyFieldMapper = dummyFieldMapper;
	}
	
	@Override
	public boolean saveAccount(AccountDto accountDto) throws TecsoCustomException {
		try {

			if (accountDto.getAccountNumber() == null || accountDto.getAccountNumber().isEmpty()) {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_EMPTY);
			}
			
			if (accountDto.getAccountNumber().length() > ACCOUNT_MAX_LENGTH) {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_LENGTH);
			}
			
			if (accountDto.getCurrencyType() == null || accountDto.getCurrencyType().isEmpty()) {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_CURRENCY_EMPTY);
			}
			
			if (CurrencyEnum.valueOf(accountDto.getCurrencyType()) == null) {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_CURRENCY_DOESNT_EXIST);
			}
			
			if (getAccountByNumber(accountDto.getAccountNumber())) {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_NUMBER_EXISTS);
			}
			
			if (accountDto.getBalance() == null) {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_BALANCE_EMPTY);
			}

			if (accountDto.getBalance() < 0) {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_BALANCE_WRONG_VALUE);
			}

			Account account = dummyFieldMapper.map(accountDto, Account.class);
			account.setCurrencyType(CurrencyEnum.valueOf(accountDto.getCurrencyType()));
			account.setCreationDate(DateUtils.getDateUTCFormat());
			
			jpaAccountRepository.save(account);
			
		} catch (IllegalArgumentException e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_TYPE_DOESNT_EXIST);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(e.getMessage());
		}
		return true;
	}
	

	@Override
	public boolean getAccountByNumber(String accountNumber) {
		
		boolean accountExist = false;
		List<Account> accounts = jdbcAccountRepository.getAccountByNumber(accountNumber);
		if (accounts != null && !accounts.isEmpty()) {
			accountExist = true;
		}
		return accountExist;
		
	}

	@Override
	public List<AccountDto> getAccountList() throws TecsoCustomException {		
		List<Account> accounts = jdbcAccountRepository.getAllAccounts();
		List<AccountDto> accountDtos = new ArrayList<AccountDto>();
		
		accounts.forEach(account ->{
			AccountDto dto = dummyFieldMapper.map(account, AccountDto.class);
			dto.setCurrencyTypeDesc(account.getCurrencyType().name());			
			accountDtos.add(dto);
		});
		
		return accountDtos;
	}

	@Override
	public boolean deleteAccount(Long id) throws TecsoCustomException {
		try {
			Optional<Account> account = jpaAccountRepository.findById(id);
			if(account.isPresent()) {				
				if(jdbcAccountRepository.countAccountMovement(account.get().getIdaccount())){
					throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_HAS_MOVEMENTS);
				}
				jpaAccountRepository.deleteById(account.get().getIdaccount());
			} else {
				throw new TecsoCustomException(AccountValidationMessages.ACCOUNT_DOESNT_EXIST);
			}		
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TecsoCustomException(e);
		}
		return true;
	}

}
