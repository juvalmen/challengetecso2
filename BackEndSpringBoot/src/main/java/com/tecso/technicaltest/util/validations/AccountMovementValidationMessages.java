package com.tecso.technicaltest.util.validations;

/**
 * 
 * @author Julian Valencia
 * 26/05/2019
 */
public class AccountMovementValidationMessages {
	
	// AccountMovement
	public static final String ACCOUNT_ID_NULL = "La cuenta es obligatoria.";
	public static final String ACCOUNT_DOESNT_EXIST = "La cuenta ingresada no existe.";
	public static final String MOVEMENT_DESCRIPTION_LENGTH = "La descripción del movimiento debe de ser máximo de 200 caracteres.";
	public static final String MOVEMENT_DESCRIPTION_EMPTY = "La descripción del movimiento no debe ser nula.";
	public static final String MOVEMENT_WRONG_VALUE = "El valor del movimiento no debe ser negativo o nulo.";
	public static final String MOVEMENT_TYPE_EMPTY = "El tipo de movimiento es obligatorio.";
	public static final String MOVEMENT_TYPE_DOESNT_EXIST = "El tipo de movimiento ingresado no existe.";
	public static final String MOVEMENT_MAX_VALUE_DOLAR = "Se ha rechazado la transacción ya que ha superado el valor máximo de descubierto de 300 dolares.";
	public static final String MOVEMENT_MAX_VALUE_EURO = "Se ha rechazado la transacción ya que ha superado el valor máximo de descubierto de 150 euros.";
	public static final String MOVEMENT_MAX_VALUE_PESO = "Se ha rechazado la transacción ya que ha superado el valor máximo de descubierto de 1000 pesos.";
	
	
}
