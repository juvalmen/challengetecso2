package com.tecso.technicaltest.util.validations;

/**
 * 
 * @author Julian Valencia
 * 26/05/2019
 */
public class AccountValidationMessages {
	
	// Account
	public static final String ACCOUNT_EMPTY = "El número de cuenta es obligatorio.";
	public static final String ACCOUNT_LENGTH = "El número de cuenta debe ser máximo de 10 caracteres.";
	public static final String ACCOUNT_CURRENCY_EMPTY = "El tipo de moneda es obligatorio.";
	public static final String ACCOUNT_NUMBER_EXISTS = "El número de cuenta ya existe.";
	public static final String ACCOUNT_TYPE_DOESNT_EXIST = "El tipo de moneda ingresado no existe.";
	public static final String ACCOUNT_BALANCE_EMPTY = "El valor del monto por número de cuenta no debe ser nulo.";
	public static final String ACCOUNT_BALANCE_WRONG_VALUE = "El valor del monto por número de cuenta no debe ser menor a cero.";
	public static final String ACCOUNT_HAS_MOVEMENTS = "No es posible borrar la cuenta ya que posee movivmientos.";
	public static final String ACCOUNT_DOESNT_EXIST = "La cuenta ingresada no existe.";
	public static final String ACCOUNT_CURRENCY_DOESNT_EXIST = "El tipo de moneda ingresado no existe.";
	
}
