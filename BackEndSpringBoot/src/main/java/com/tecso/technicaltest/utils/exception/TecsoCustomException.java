package com.tecso.technicaltest.utils.exception;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
public class TecsoCustomException extends Exception {

	private static final long serialVersionUID = -940541417432082672L;

	public TecsoCustomException(String message) {
        super(message);
    }
    
    public TecsoCustomException(Throwable throwable){
    	super(throwable);
    }
    
    public TecsoCustomException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
