CREATE SCHEMA IF NOT EXISTS `tecsobdtechnicaltesttwo` DEFAULT CHARACTER SET utf8 ;
USE `tecsobdtechnicaltesttwo` ;

CREATE TABLE account (
  `idaccount` int(11) NOT NULL AUTO_INCREMENT,
  `account_number` VARCHAR(10) NOT NULL,
  `currency_type` VARCHAR(10) NOT NULL,
  `balance`  DECIMAL(10,2) NOT NULL,
  `creation_date`  DATE NOT NULL,
  `modification_date`  DATE,
  PRIMARY KEY (`idaccount`),
  UNIQUE INDEX `account_number_UNIQUE` (`account_number` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE accountmovement (
  `idaccountmovement` int(11) NOT NULL AUTO_INCREMENT,
  `idaccount` int(11) NOT NULL, 
  `movement_date` DATE NOT NULL,
  `movement_type` VARCHAR(10) NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  `value`  DECIMAL(10,2) NOT NULL,
  `creation_date`  DATE NOT NULL,
  PRIMARY KEY (`idaccountmovement`),
  INDEX `fk_account_idx` (`idaccount` ASC),
  CONSTRAINT `fk_account_idaccount_account`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;
