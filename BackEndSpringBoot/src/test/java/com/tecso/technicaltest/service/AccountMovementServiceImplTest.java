package com.tecso.technicaltest.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.dto.AccountDto;
import com.tecso.technicaltest.dto.AccountMovementDto;
import com.tecso.technicaltest.enums.CurrencyEnum;
import com.tecso.technicaltest.enums.MovementEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Account;
import com.tecso.technicaltest.model.AccountMovement;
import com.tecso.technicaltest.repository.jdbc.JdbcAccountMovementRepository;
import com.tecso.technicaltest.repository.jpa.JpaAccountMovementRepository;
import com.tecso.technicaltest.repository.jpa.JpaAccountRepository;
import com.tecso.technicaltest.service.impl.AccountMovementServiceImpl;
import com.tecso.technicaltest.util.DateUtils;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@RunWith(SpringRunner.class)
@SpringBootTest()
@Transactional
@ActiveProfiles("test") 
public class AccountMovementServiceImplTest {
	
	@Autowired
	@InjectMocks
	private AccountMovementServiceImpl accountMovementServiceImpl;

	@Mock
	private JdbcAccountMovementRepository jdbcAccountMovementRepository;
	
	@Mock
	private JpaAccountMovementRepository jpaAccountMovementRepository;

	@Mock
	private JpaAccountRepository jpaAccountRepository;
	
	@Mock
	private DummyFieldMapper dummyFieldMapper;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * 
	 */
	public AccountDto getAccountDto() {
		AccountDto accountDto = new AccountDto();
		accountDto.setAccountNumber("1234567890");
		accountDto.setBalance(1000.00);
		accountDto.setCreationDate(DateUtils.getDateUTCFormat());
		accountDto.setCurrencyType(CurrencyEnum.DOLAR.toString());
		accountDto.setCurrencyTypeDesc(CurrencyEnum.DOLAR.name());		
		return accountDto;
	}
	
	/**
	 * 
	 */
	public Account getAccount() {
		Account account = new Account();
		account.setAccountNumber("1234567890");
		account.setBalance(1000.00);
		account.setCreationDate(DateUtils.getDateUTCFormat());
		account.setCurrencyType(CurrencyEnum.DOLAR);
		return account;
	}

	/**
	 * 
	 */
	public AccountMovement getAccountMovement() {
		AccountMovement accountMovement = new AccountMovement();
		accountMovement.setIdaccountmovement(1L);
		accountMovement.setMovementDate(DateUtils.getDateUTCFormat());
		accountMovement.setMovementType(MovementEnum.CREDITO);
		accountMovement.setDescription("Movimiento compra tiena");
		accountMovement.setMovementValue(1000000.00);
		return accountMovement;
	}

	/**
	 * 
	 */
	public AccountMovementDto getAccountMovementDto() {
		AccountMovementDto accountMovementDto = new AccountMovementDto();
		accountMovementDto.setIdaccountmovement(1L);
		accountMovementDto.setIdaccount(1L);
		accountMovementDto.setAccountNumber("123234134");
		accountMovementDto.setAccountCurrencyType(CurrencyEnum.DOLAR.name());
		accountMovementDto.setMovementDate(DateUtils.getDateUTCFormat());
		accountMovementDto.setMovementType(MovementEnum.CREDITO.name());
		accountMovementDto.setMovementTypeDesc(MovementEnum.CREDITO.name());
		accountMovementDto.setDescription("Movimiento compra tiena");
		accountMovementDto.setMovementValue(1000000.00);
		accountMovementDto.setCreationDate(DateUtils.getDateUTCFormat());
		return accountMovementDto;
	}
	
	public List<AccountMovement> getMovementsList(){
		List<AccountMovement> list = new ArrayList<AccountMovement>();
		list.add(getAccountMovement());
		return list;
	}
	
    @Test
    public void test_getMovementsByAccountEmpty() throws TecsoCustomException {
						
		when(jdbcAccountMovementRepository.getMovementsByAccount(1L)).thenReturn(new ArrayList<AccountMovement>());
		
		accountMovementServiceImpl.getMovementsByAccount(1L);
    }
    
    @Test
    public void test_getMovementsByAccountList() throws TecsoCustomException {
    	
    	List<AccountMovement> accountMovements = new ArrayList<AccountMovement>();
		Account account = getAccount();
		account.setIdaccount(1L);
		AccountMovement accountMovement = getAccountMovement();
		accountMovement.setIdaccountmovement(1L);
		accountMovement.setAccount(account);
    	accountMovements.add(accountMovement);
    	
		when(jdbcAccountMovementRepository.getMovementsByAccount(1L)).thenReturn(accountMovements);
		
		when(dummyFieldMapper.map(accountMovement, AccountMovementDto.class)).thenReturn(getAccountMovementDto());
		
		accountMovementServiceImpl.getMovementsByAccount(1L);
    }
    

    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountBalanceIdAccountNull() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();

    	accountMovementDto.setIdaccount(null);
    	
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountBalanceIdAccountDoesntExist() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();
    	
    	when(jpaAccountRepository.findById(1L)).thenReturn(Optional.of(new Account()));
    	
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    
    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountDescriptionNullOrEmpty() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();
    	accountMovementDto.setDescription(null);
    	
    	when(jpaAccountRepository.findById(1L)).thenReturn(Optional.of(new Account()));
    	
		when(dummyFieldMapper.map(new Account(), AccountDto.class)).thenReturn(getAccountDto());
    	
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountDescriptionLentgh() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();
    	accountMovementDto.setDescription("Contadordecaracteres.com es un contador automático de caracteres y palabras en un texto. Solo colocque el cursor dentro de la caja de textos y comienze a escribir y el contador de caracteres comenzara a contar sus palabras y caracteres a medida de que usted vaya escribiendo. También puede copiar y pegar bloques de texto de un documento que tengas ya escrito. Solo pegue el texto dentro de la caja del contador y rápidamente se mostrara la cantidad de caracteres y palabras que contenga el texto pegado.");
    	
    	when(jpaAccountRepository.findById(1L)).thenReturn(Optional.of(new Account()));
    	
		when(dummyFieldMapper.map(new Account(), AccountDto.class)).thenReturn(getAccountDto());
    	
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountMovementValueNull() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();
    	accountMovementDto.setMovementValue(null);
    	
    	when(jpaAccountRepository.findById(1L)).thenReturn(Optional.of(new Account()));
    	
		when(dummyFieldMapper.map(new Account(), AccountDto.class)).thenReturn(getAccountDto());
    	
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountMovementTypeNull() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();
    	accountMovementDto.setMovementType(null);
    	
    	when(jpaAccountRepository.findById(1L)).thenReturn(Optional.of(new Account()));
    	
		when(dummyFieldMapper.map(new Account(), AccountDto.class)).thenReturn(getAccountDto());
    	
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountMovementTypeDoesntExist() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();
    	accountMovementDto.setMovementType("BOLIVAR");
    	
    	when(jpaAccountRepository.findById(1L)).thenReturn(Optional.of(new Account()));
    	
		when(dummyFieldMapper.map(new Account(), AccountDto.class)).thenReturn(getAccountDto());
    	
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_updateAccountMovementFullCredito() throws TecsoCustomException {
    	
    	AccountMovementDto accountMovementDto = getAccountMovementDto();
    	
    	when(jpaAccountRepository.findById(1L)).thenReturn(Optional.of(getAccount()));
        	
		when(dummyFieldMapper.map(new AccountDto(), Account.class)).thenReturn(getAccount());

		when(dummyFieldMapper.map(Optional.of(new Account()), AccountDto.class)).thenReturn(getAccountDto());
		
		when(dummyFieldMapper.map(new AccountMovementDto(), AccountMovement.class)).thenReturn(getAccountMovement());
		
    	accountMovementServiceImpl.updateAccountBalance(accountMovementDto);
    }
    
    

}
