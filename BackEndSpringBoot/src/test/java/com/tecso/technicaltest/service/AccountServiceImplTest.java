package com.tecso.technicaltest.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.dto.AccountDto;
import com.tecso.technicaltest.enums.CurrencyEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Account;
import com.tecso.technicaltest.repository.jdbc.JdbcAccountRepository;
import com.tecso.technicaltest.repository.jpa.JpaAccountRepository;
import com.tecso.technicaltest.service.impl.AccountServiceImpl;
import com.tecso.technicaltest.util.DateUtils;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@RunWith(SpringRunner.class)
@SpringBootTest()
@Transactional
@ActiveProfiles("test") 
public class AccountServiceImplTest {
	
	@Autowired
	@InjectMocks
	private AccountServiceImpl accountServiceImpl;

	@Mock
	private JdbcAccountRepository jdbcAccountRepository;
	
	@Mock
	private JpaAccountRepository jpaAccountRepository;
	
	@Mock
	private DummyFieldMapper dummyFieldMapper;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * 
	 */
	public AccountDto getAccountDto() {
		AccountDto accountDto = new AccountDto();
		accountDto.setAccountNumber("1234567890");
		accountDto.setBalance(1000.00);
		accountDto.setCreationDate(DateUtils.getDateUTCFormat());
		accountDto.setCurrencyType(CurrencyEnum.DOLAR.toString());
		accountDto.setCurrencyTypeDesc(CurrencyEnum.DOLAR.name());		
		return accountDto;
	}
	
	/**
	 * 
	 */
	public Account getAccount() {
		Account account = new Account();
		account.setAccountNumber("1234567890");
		account.setBalance(1000.00);
		account.setCreationDate(DateUtils.getDateUTCFormat());
		account.setCurrencyType(CurrencyEnum.DOLAR);
		return account;
	}
	
	/**
	 * 
	 */
	public List<AccountDto> getListAccountDto() {
		List<AccountDto> list = new ArrayList<AccountDto>();
		AccountDto account1 = getAccountDto(); 
		account1.setIdaccount(1L);
		account1.setAccountNumber("33333333");
		AccountDto account2 = getAccountDto(); 
		account2.setIdaccount(2L);
		account2.setAccountNumber("4444444");
		account2.setCurrencyType(CurrencyEnum.EURO.toString());
		AccountDto account3 = getAccountDto(); 
		account3.setIdaccount(3L);
		account3.setAccountNumber("555555555");
		account3.setCurrencyType(CurrencyEnum.PESO.toString());
	
		list.add(account1);
		list.add(account2);
		list.add(account3);
		return list;
	}
	
	/**
	 * 
	 */
	public List<Account> getListAccount() {
		List<Account> list = new ArrayList<Account>();
		Account account1 = getAccount(); 
		account1.setIdaccount(1L);
		account1.setAccountNumber("33333333");
		Account account2 = getAccount(); 
		account2.setIdaccount(2L);
		account2.setAccountNumber("4444444");
		Account account3 = getAccount(); 
		account3.setIdaccount(3L);
		account3.setAccountNumber("555555555");
	
		list.add(account1);
		list.add(account2);
		list.add(account3);
		return list;
	}
	
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberNull() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
				
			accountDto.setAccountNumber(null);
					
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberEmpty() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
				
			accountDto.setAccountNumber("");
					
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberLentgh() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
				
			accountDto.setAccountNumber("12345678901234567890");
					
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberCurrencyTypeNull() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
			
			accountDto.setCurrencyType(null);
			
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberCurrencyTypeEmpty() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
			
			accountDto.setCurrencyType("");
			
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberCurrencyTypeDoesntExist() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
			
			accountDto.setCurrencyType("BOLIVAR");
			
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberExist() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
			account.setIdaccount(1L);
			
			when(jdbcAccountRepository.getAccountByNumber(accountDto.getAccountNumber())).thenReturn(getListAccount());
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    
    @Test
    public void test_AccountNumberDoenstExist() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
			
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberNegativeBalance() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
			
			accountDto.setBalance(-10000.00);
			
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test(expected = TecsoCustomException.class)
    public void test_AccountNumberNullBalance() throws TecsoCustomException {
			Account account = getAccount();
			AccountDto accountDto = getAccountDto();
			
			accountDto.setBalance(null);
			
			when(jdbcAccountRepository.getAccountByNumber("1234567890")).thenReturn(null);
		
			when(dummyFieldMapper.map(accountDto, Account.class)).thenReturn(account);
			
			accountServiceImpl.saveAccount(accountDto);
    }
    
    @Test
    public void test_getListAccountEmpty() throws TecsoCustomException {
    	Account account = getAccount();
		AccountDto accountDto = getAccountDto();
    	
		when(jdbcAccountRepository.getAllAccounts()).thenReturn(new ArrayList<Account>());
		
		when(dummyFieldMapper.map(account, AccountDto.class)).thenReturn(accountDto);
		
		assertEquals(true, new ArrayList<Account>().isEmpty() ? true : false);
    }
   

}
